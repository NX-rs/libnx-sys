extern crate bindgen;

use std::env;
use std::path::PathBuf;

use bindgen::EnumVariation;

pub fn main() {
    let devkitpro_path = env::var("DEVKITPRO").expect("$DEVKITPRO not set (set to devkitpro path, typically `/opt/devkitpro`)");

    let bindings = bindgen::Builder::default()
        .clang_arg(format!("-I{}/libnx/include", devkitpro_path))
        .clang_arg(format!("-I{}/devkitA64/aarch64-none-elf/include", devkitpro_path))
        
        .default_enum_style(EnumVariation::NewType { is_bitfield: true })
        .generate_inline_functions(true)
        .trust_clang_mangling(false)
        
        .blacklist_type("u8")
        .blacklist_type("u16")
        .blacklist_type("u32")
        .blacklist_type("u64")    
        
        .header("libnx.h")
        
        .generate()
        .expect("Failed to generate libnx bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("libnx.rs"))
        .expect("Failed to write bindings to libnx.rs");
}